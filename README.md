# Desafio Bossabox - NODEJS

 + Aplicação desenvolvida em nodejs, para resolver o desafio proposto pela bossa bossabox, em desenvolver uma API de cadastro de Tools.
 
 # Libs Utilizadas
   -> JWT (JsonWebToken): Utilizado para Autenticação e Autorização do Usuário

   -> Mongoose: ORM para trabalhar com o monogoDB, como base de dados
    
   -> mocha: Lib para realizar testes da aplicação
   
  
 # Iniciando Projeto
    git clone
    npm install
    npm run start 
    
    #test
    npm run test

# link da aplicação heroku: https://desafiobossabox.herokuapp.com/
