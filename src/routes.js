const { Router } = require("express");

const ToolsController = require("./app/controllers/ToolsControllers");
const UserController = require("./app/controllers/UserController");
const SessionController = require("./app/controllers/SessionController");

const authConfig = require("./app/middlewares/auth");

const routes = new Router();

routes.post("/users", UserController.store);
routes.post("/session", SessionController.store);
routes.get("/users", UserController.index);

routes.use(authConfig);

routes.post("/tools", ToolsController.store);
routes.get("/tools", ToolsController.index);
routes.get("/tool", ToolsController.show);

routes.delete("/tools/:id", ToolsController.remove);
module.exports = routes;
