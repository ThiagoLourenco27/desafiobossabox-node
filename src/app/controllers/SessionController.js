const UserModel = require("../models/User");
const jwt = require("jsonwebtoken");
const bcrypt = require("bcryptjs");
const authConfig = require("../../config/auth");

function generateToken(params = {}) {
  return jwt.sign(params, authConfig.secret, {
    expiresIn: 86400,
  });
}

class SessionController {
  async store(req, res) {
    const { email, password } = req.body;

    const user = await UserModel.findOne({ email });

    if (!user) {
      return res.status(400).send({ error: "User not found :( " });
    }

    if (!(await bcrypt.compare(password, user.password))) {
      return res.status(400).send({ error: "Incorrect password" });
    }

    res.json({
      user: user,
      token: generateToken({ id: user.id }),
    });
  }
}

module.exports = new SessionController();
