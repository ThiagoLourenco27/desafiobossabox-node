const UserModel = require("../models/User");

class UserController {
  async index(req, res) {
    try {
      const response = await UserModel.find();

      if (response.length === 0) {
        return res.status(400).json({ message: "Não tem usuários cadastrado" });
      }

      return res.json(response);
    } catch (error) {
      return res.status(401).json({ message: "Server Invalid" });
    }
  }

  async store(req, res) {
    try {
      const { email } = req.body;

      const userExists = await UserModel.findOne({ email });

      if (userExists) {
        return res.json({ message: "Usuário Já existe" });
      }
      const response = await UserModel.create(req.body);

      return res.json(response);
    } catch (error) {
      return res.status(401).json({ message: "Server Invalid" });
    }
  }
}

module.exports = new UserController();
