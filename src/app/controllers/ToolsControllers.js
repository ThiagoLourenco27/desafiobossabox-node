const ToolModel = require("../models/Tools");

class ToolsController {
  async index(req, res) {
    try {
      const response = await ToolModel.find();

      return res.status(200).json(response);
    } catch (error) {
      return res.status(401).json({ message: "Server Invalid" });
    }
  }

  async store(req, res) {
    try {
      const response = await ToolModel.create(req.body);

      return res.status(201).json(response);
    } catch (error) {
      return res.status(401).json({ message: "Server Invalid" });
    }
  }

  async show(req, res) {
    try {
      const response = await ToolModel.find({ tags: req.query.tag });

      return res.status(200).json(response);
    } catch (error) {
      return res.status(401).json({ message: "Server Invalid" });
    }
  }

  async remove(req, res) {
    try {
      const response = await ToolModel.findOneAndDelete(req.params.id);

      return res.status(204).json({
        message: "Tool removed successfully.",
        tools: response,
      });
    } catch (error) {
      return res.status(401).json({ message: "Server Invalid" });
    }
  }
}

module.exports = new ToolsController();
