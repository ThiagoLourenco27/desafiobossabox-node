// MOCHA

const assert = require("assert");
const mongoose = require("mongoose");

const ToolModel = require("../app/models/Tools");

const MOCK_TOOL = {
  title: "Flutter",
  link: "https://www.google.com",
  description: "Tecnologia para desenvolver aplativos multiplataforma",
  tags: ["Dart", "Flutter"],
};

describe("Suite de Teste API de Tools", function () {
  this.beforeAll(async () => {
    mongoose.connect(
      "mongodb+srv://bossabox:tl123456@cluster0-4rfre.mongodb.net/bossaboxtest?retryWrites=true&w=majority",
      { useUnifiedTopology: true, useNewUrlParser: true }
    );
  });

  it("cadastrar tool", async () => {
    const { title, link, description, tags } = await ToolModel.create(
      MOCK_TOOL
    );

    assert.deepEqual({ title, link, description, tags }, MOCK_TOOL);
  });

  it("listar todas as tools", async () => {
    const response = await ToolModel.find();

    const tamanho = response.length > 0;

    assert.ok(response);
  });

  it("filtrar uma determinada tool por tag", async () => {
    const tag = "JavaScript";
    const response = await ToolModel.find({ tags: tag });

    assert.ok(response);
  });

  it("Remover um determinado tool por id", async () => {
    const response = await ToolModel.findOneAndDelete({
      _id: "5ef386f1df89f60016f56b66",
    });

    assert.ok(response);
  });
});
// //
