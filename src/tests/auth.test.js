const assert = require("assert");
const mongoose = require("mongoose");
const bcrypt = require("bcryptjs");

const UserModel = require("../app/models/User");

const MOCK_USER = {
  name: "Thiago",
  email: "sss@gmail.com",
  password: "123456",
};

const MOCK_AUTH = {
  email: "sss@gmail.com",
  password: "123456",
};

describe("Suite de Teste API de USER AUTH", function () {
  this.beforeAll(async () => {
    mongoose.connect(
      "mongodb+srv://bossabox:tl123456@cluster0-4rfre.mongodb.net/bossaboxtest?retryWrites=true&w=majority",
      { useUnifiedTopology: true, useNewUrlParser: true }
    );
  });

  it("criar usuário", async () => {
    try {
      const response = await UserModel.create(MOCK_USER);

      assert.ok(response);
    } catch (err) {
      assert.ok(err);
    }
  });

  it.only("list users", async () => {
    const response = await UserModel.find();

    assert.ok(response);
  });
});
