const express = require("express");
const cors = require("cors");
const mongoose = require("mongoose");

const routes = require("./routes");

class Server {
  constructor() {
    this.app = express();

    this.middlewares();
    this.database();
    this.routes();
  }

  middlewares() {
    this.app.use(express.json());
    this.app.use(cors());
  }

  database() {
    mongoose.connect(
      "mongodb+srv://bossabox:tl123456@cluster0-4rfre.mongodb.net/bossabox?retryWrites=true&w=majority",
      { useUnifiedTopology: true, useNewUrlParser: true, useCreateIndex: true }
    );

    console.log("CONECTION DATA BASE");
  }

  routes() {
    this.app.use(routes);
  }
}

module.exports = new Server().app;
