FORMAT: 1A
URL: https://desafiobossabox.herokuapp.com/

# Desafio BossaBox

Realizar o desenvolvimento de uma api rest, para cadastrar, listar e remover uma determinada ferramentar. Nesta api também foi implementado Authentication

## Listagem de Ferramentas [GET]

## Listagem de Todas as Ferramenas [/tools]

    + Response 200 (application/json)

        [
            {
                "tags": [
                "JavaScript",
                "Dart",
                "React Native",
                "facebook"
                ],
                "_id": "5ef35be8aa0b9979361c8314",
                "title": "React Native melhor que Fluuter",
                "link": "https://www.google.com",
                "description": "Comm o passar do tempo, percebemos que o react native está em uma curva de aprendizado bem melhor, do que o flutter",
                "createdAt": "2020-06-24T13:58:00.839Z",
                "updatedAt": "2020-06-24T13:58:00.839Z",
                "__v": 0
            },
            {
                "tags": [
                "Php"
                ],
                "_id": "5ef35c48777c417b928ccd7b",
                "title": "Php",
                "link": "https://www.google.com",
                "description": "Comm o passar do tempo, percebemos que o react native está em uma curva de aprendizado bem melhor, do que o flutter",
                "createdAt": "2020-06-24T13:59:36.508Z",
                "updatedAt": "2020-06-24T13:59:36.508Z",
                "__v": 0
            }
        ]

## Filtragem das Ferramentas [/tool?tag=JavaScript]

    + Response 200 (application/json)
        [
            {
                "tags": [
                "JavaScript",
                "Dart",
                "React Native",
                "facebook"
                ],
                "_id": "5ef35be8aa0b9979361c8314",
                "title": "React Native melhor que Fluuter",
                "link": "https://www.google.com",
                "description": "Comm o passar do tempo, percebemos que o react native está em uma curva de aprendizado bem melhor, do que o flutter",
                "createdAt": "2020-06-24T13:58:00.839Z",
                "updatedAt": "2020-06-24T13:58:00.839Z",
                "__v": 0
            }
        ]

## Criar uma nova ferramena [POST]

## Criar uma ferramenta [/tools]

    + Response 201 (application/json)

        {
            "tags": [
                "Dart",
                "Flutter"
            ],
            "_id": "5ef363f82f067735ef5be5cb",
            "title": "Flutter",
            "link": "https://www.google.com",
            "description": "Tecnologia para desenvolver aplativos multiplataforma",
            "createdAt": "2020-06-24T14:32:24.592Z",
            "updatedAt": "2020-06-24T14:32:24.592Z",
            "__v": 0
            }

## Remover Ferramenta [DELETE]

## Remover uma determinada ferramenta [/tools/:id]

        + Response 204 (application/json)

## Cadastro de um novo usuário [POST][/users]

    + Response 201 (application/json)

    {
        "name": "Thiago",
        "email": "thiagolourenco@gmail.com",
        "password": "123456"
    }

## Login do Usuário [POST][/session]

    {
        "email": "thiagolourenco@gmail.com",
        "password": "123456"
    }
